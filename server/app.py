# -*- coding: utf-8 -*-
import sqlite3
from bottle import route, run, request, response


@route('/get/<datatype>/<limit:int>')
def get_data(datatype, limit):
    conn = sqlite3.connect('base.db')
    c = conn.cursor()
    c.execute("select * from contact limit %s" % limit)
    data = c.fetchall()
    json = {}
    xml = '<data>'

    if datatype == 'xml':
        for item in data:
            reg = '<id>%s</id><name>%s</name><email>%s</email><date>%s</date>' % \
                    (item[1], item[2], item[3], item[0])
            xml += '<item>%s</item>' % reg
        xml += '</data>'
        response.headers['Content-Type'] = 'xml/application'
        return xml
    elif datatype == 'json':
        for item in data:
            json[item[2]] = item
        return json
    else:
        return 'Escolha um formato disponível: xml ou json'


run(server='gunicorn', port=8001, reloader=True)
