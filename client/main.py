# -*- coding: utf-8 -*-
from Tkinter import *
import requests

SERVER = 'http://raelmax.com/get'

class App:
    def __init__(self, master):
        self.qtd_reqs = 0

        frame = Frame(master)
        frame.pack()
        self.txtfr(frame)

        self.json = Button(frame, text="Requisitar JSON", command=self.get_json)
        self.json.pack(side=LEFT)

        self.xml = Button(frame, text="Requisitar XML", command=self.get_xml)
        self.xml.pack(side=LEFT)

    def get_json(self):
        req = requests.get('%s/json/100' % SERVER)
        self.text.delete(1.0, END)
        self.text.insert(INSERT, req.text)

    def get_xml(self):
        req = requests.get('%s/xml/100' % SERVER)
        self.text.delete(1.0, END)
        self.text.insert(INSERT, req.text)

    def txtfr(self,frame):
        
        #define a new frame and put a text area in it
        textfr=Frame(frame)
        self.text=Text(textfr,height=30,width=80,background='white')
        
        # put a scroll bar in the frame
        scroll=Scrollbar(textfr)
        self.text.configure(yscrollcommand=scroll.set)
        
        #pack everything
        self.text.pack(side=LEFT)
        scroll.pack(side=RIGHT,fill=Y)
        textfr.pack(side=TOP)
        return

root = Tk()
app = App(root)
root.title('Cliente')
root.mainloop()
